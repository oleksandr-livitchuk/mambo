'use strict';

const path = require('path'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    debug = require('gulp-debug'),
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    svgSprite = require('gulp-svg-sprite'),
    gulpIf = require('gulp-if'),
    cssnano = require('gulp-cssnano'),
    rev = require('gulp-rev'),
    revReplace = require('gulp-rev-replace'),
    combine = require('stream-combiner2').obj,
    fs = require('fs'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    prefixer = require('gulp-autoprefixer'),
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify'),
    fontmin = require('gulp-fontmin'),
    logger = require('fancy-log'),
    vfs = require('vinyl-fs'),
    converter = require('sass-convert'),
    sassdoc = require('sassdoc'),
    spritesmith = require('gulp.spritesmith');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

let dir = {
    www: {
        assets: 'www/',
        js: 'www/js/',
        css: 'www/styles/',
        img: {
            assets: 'www/images/'
        },
        fonts: 'www/fonts/'
    },
    frontend: {
        assets: 'frontend/assets/*.*',
        html: 'frontend/assets/html/**/*.html',
        js: {
            vendor: 'frontend/js/vendor/*.js',
            build: 'frontend/js/*.js'
        },
        styles: 'frontend/styles/main.scss',
        sprites: 'frontend/styles/',
        img: {
            sprites: 'frontend/images/sprites/**/*.*',
            assets: 'frontend/images/**/*.*'
        },
        fonts: 'frontend/fonts/**/*.*',
        manifest: 'www/css-manifest/'
    },
};

gulp.task('sassdoc', function () {
    return gulp.src('frontend/styles/*.scss')
        .pipe(converter({
            from: 'scss',
            to: 'sass',
        }))
        .pipe(sassdoc());
});

gulp.task('styles', function () {

    return gulp.src(dir.frontend.styles)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Styles',
                message: err.message
            }))
        }))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass())
        .pipe(gulpIf(!isDevelopment, combine(prefixer(), cssnano(), rev())))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(dir.www.css))
        .pipe(gulpIf(!isDevelopment, combine(rev.manifest('css.json'), gulp.dest(dir.frontend.manifest))));
});

gulp.task('js:build', function () {
    return gulp.src(dir.frontend.js.build)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Javascript',
                message: err.message
            }))
        }))
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dir.www.js))
});

gulp.task('js:vendor', function () {
    return gulp.src(dir.frontend.js.vendor)
        .pipe(gulp.dest(dir.www.js))
});

gulp.task('clean', function () {
    return del([dir.www.assets, dir.frontend.manifest]);
});
gulp.task('clog', function () {
    return console.log(dir.frontend.html);
});

gulp.task('assets', function () {
    return gulp.src(dir.frontend.assets, {since: gulp.lastRun('assets')})
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Assets',
                message: err.message
            }))
        }))
        .pipe(gulp.dest(dir.www.assets));
});

gulp.task('html', function () {
    return gulp.src(dir.frontend.html)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'HTML',
                message: err.message
            }))
        }))
        .pipe(rigger())
        .pipe(gulpIf(!isDevelopment, revReplace({
            manifest: gulp.src(dir.frontend.manifest + 'css.json', {allowEmpty: true})
        })))
        .pipe(gulp.dest(dir.www.assets));
});

gulp.task('image:assets', function () {
    return gulp.src(dir.frontend.img.assets, {since: gulp.lastRun('image:assets')})
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Image Assets',
                message: err.message
            }))
        }))
        // .pipe(gulpIf(!isDevelopment, imagemin([
        //     imagemin.gifsicle({interlaced: true}),
        //     imagemin.jpegtran({progressive: true}),
        //     imagemin.optipng({optimizationLevel: 5}),
        //     imagemin.svgo({
        //         plugins: [
        //             {removeViewBox: true},
        //             {cleanupIDs: false}
        //         ]
        //     })
        // ])))
        .pipe(gulp.dest(dir.www.img.assets));
});

gulp.task('sprites', function () {
    return gulp.src(dir.frontend.img.sprites)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Sprites',
                message: err.message
            }))
        }))
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: '_sprite.scss',
            padding: 2,

        }))
        .pipe(gulpIf('*.scss', gulp.dest(dir.frontend.sprites), gulp.dest(dir.www.css)));
});

gulp.task('fonts', function () {
    return gulp.src(dir.frontend.fonts)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Fonts',
                message: err.message
            }))
        }))
        .pipe(gulpIf(!isDevelopment, fontmin()))
        .pipe(gulp.dest(dir.www.fonts))
});

gulp.task('deploy', function () {

    var conn = ftp.create({
        host: '',
        user: '',
        password: '',
        parallel: 10,
        log: logger(log)
    });

    var globs = [
        'www/css/styles.min.css'
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src(globs, {base: '.', buffer: false})
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'FTP',
                message: err.message
            }))
        }))
        .pipe(conn.newer('/itrend.com.ua/fitness/public')) // only upload newer files
        .pipe(conn.dest('/itrend.com.ua/fitness/public'));

});

gulp.task('build', gulp.series('clean', 'image:assets', 'sprites', 'styles', 'js:vendor', 'js:build', 'assets', 'fonts', 'html'));

gulp.task('watch', function () {
    gulp.watch(dir.frontend.styles, gulp.series('styles'));
    gulp.watch(dir.frontend.assets, gulp.series('assets'));
    gulp.watch(dir.frontend.html, gulp.series('html'));
    gulp.watch(dir.frontend.fonts, gulp.series('fonts'));
    gulp.watch(dir.frontend.img.sprites, gulp.series('sprites'));
    gulp.watch(dir.frontend.img.assets, gulp.series('image:assets'));
    gulp.watch(dir.frontend.js.build, gulp.series('js:build'));
});

gulp.task('serve', function () {
    browserSync.init({
        server: 'www/'
    });

    browserSync.watch('www/**/*.*').on('change', browserSync.reload);
});

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));

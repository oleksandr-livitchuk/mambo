// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

////******IMPORT LIBRARIES ********\\\\\\
//= lightslider.js
//= background-check.js


$("document").ready(function () {

    new WOW().init();

    $("#lightSlider").lightSlider({
        gallery: true,
        item: 1,
        slideMove: 1, // slidemove will be 1 if loop is true
        galleryMargin: 20,
        slideMargin: 0,
        thumbItem: 4,
        thumbMargin: 10,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    item: 2,
                    slideMargin: 10,
                }
            }
        ]
    });

    $("#modalSlider").lightSlider({
        item: 1,
        slideMove: 1, // slidemove will be 1 if loop is true
        galleryMargin: 20,
        slideMargin: 0,
        adaptiveHeight: true
    });

    $('.product__count').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.count-up'),
            btnDown = spinner.find('.count-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });

    $(".catalog-dropdown__btn").click(function () {
        $(".catalog-dropdown").toggleClass("active");
    })


    $(".menu-btn").click(function () {
        $(this).toggleClass('open');
        $(".side-menu").toggleClass('open');
    });

    $(".side-menu__link--catalog").click(function (e) {
        $(this).toggleClass('open');
        $('.side-menu__catalog').toggleClass('open');
        e.preventDefault();
    });

    $(".search-field__field").each(function () {
        var parent_width = $(".header-mobile").width(),
            logout_width = $(".header-mobile__logout").width(),
            cart_width = $(".header-mobile__cart").width();

        $(this).width(parent_width - logout_width - cart_width - 10);
    });

    $("#search_toggle").click(function () {
        $("#search_box").addClass("open");
        $(".header-mobile__left").addClass("transparent");
    });

    $("#search_back").click(function () {
        $("#search_box").removeClass("open");
        $(".header-mobile__left").removeClass("transparent");
    });

    $(".lslide").click(function () {
        if ($(window).width() <= 768) {
            $(".modal-gallery").addClass("open");
        }
    });

    $("#modal-close").click(function () {
        $(".modal-gallery").removeClass("open");
    });

    $(".size-table-btn__button").click(function (e) {
        $(".modal-sizes").addClass("open");
        e.preventDefault();
    });

    $("#sizes-close").click(function () {
        $(".modal-sizes").removeClass("open");
    });

    $(".callback-btn").click(function (e) {
        $(".modal-callback").addClass("open");
        e.preventDefault();
    });

    $("#callback-close").click(function () {
        $(".modal-callback").removeClass("open");
    });

    $(".cart-button").click(function () {
        $(".cart-dropdown").addClass("open");
    });

    $(document).mouseup(function (e) {
        var cart_dropdown =  $(".cart-dropdown");

        // if the target of the click isn't the container nor a descendant of the container
        if (!cart_dropdown.is(e.target) && cart_dropdown.has(e.target).length === 0) {
            cart_dropdown.removeClass("open");
        }
    });

    $(".call-block").hover(function () {
        $(".call-block__icon").addClass("rubberBand");
        setTimeout(function () {
            $(".call-block__icon").removeClass("rubberBand");
        }, 400);
    });

});

BackgroundCheck.init({
    targets: '.product-card__name, .product-card__price',
    images: '.product-card__bg img'
});

if ($(".carousel").length) {
    var slideIndex = 1;
    showSlides(slideIndex);

// Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

// Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "flex";
        dots[slideIndex - 1].className += " active";
    }
}


